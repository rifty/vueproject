var path = require('path');
var webpack = require("webpack");
// var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CompressionWebpackPlugin = require('compression-webpack-plugin');
var OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin');
var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
var opn = require('opn');

var chalk = require('chalk');
var shell = require('shelljs');

var WebpackDevServer = require("webpack-dev-server");


var dir_root = path.resolve(__dirname); // 项目目录
var dir_web = path.join(dir_root, "src");   // 开发目录
var dir_resource_css = path.join(dir_root, "dist/css"); // 生成css的目录
var dir_resource_js = path.join(dir_root, "dist/js");   // 生成js的目录


process.env.NODE_ENV = 'development';

var webpackConfig = {
    entry: {
        app: "./src/main.js"
    },
    output: {
        path: path.resolve(__dirname, ''),// 绝对路径，一般指定项目总目录为输出目录，然后对输出的文件进行自定义
        filename: "dist/js/[name].bundle.js",// 每个输出 bundle 的名称，开发阶段不需要hash
        publicPath: "http://localhost:3001/", // 对于按需加载(on-demand-load)或加载外部资源(external resources)（如图片、文件等）,此选项的值都会以/结束
        chunkFilename: "dist/js/[name].bundle.js",// 按需加载(on-demand loaded)的 chunk 文件的名称，开发阶段不需要hash
        jsonpFunction: 'WP',// 用于按需加载(load on-demand) chunk 的 JSONP 函数
    },
    module: {
        rules: [

            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        scss: 'vue-style-loader!css-loader!sass-loader', // <style lang="scss">// 开发阶段不需要压缩
                        sass: 'vue-style-loader!css-loader!sass-loader' // <style lang="sass">// 开发阶段不需要压缩
                    }
                }
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader' // 开发阶段不需要压缩
                    },
                    {
                        loader: 'sass-loader'
                    }
                ]
            },
            {
                test: /\.js$/,
                include: /src/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            // url-loader 需要file-loader，当文件超过limit指定的大小是，会自动使用file-loader
            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                loader: 'url-loader',
                options: {
                    limit: 8192, // 8kb(8*1024)
                    name: 'dist/images/[name].[hash:7].[ext]'
                }
            },
            // {
            //     test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
            //     loader: 'url-loader',
            //     query: {
            //         limit: 10000,
            //         name: path.resolve(__dirname, 'dist/images/[name].[hash:7].[ext]')
            //     }
            // }


        ]
    },

    plugins: [

        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: function (module) {
               // 该配置假定你引入的 vendor 存在于 node_modules 目录中
               return module.context && module.context.indexOf( path.join(__dirname, 'node_modules') ) !== -1;
            }
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'manifest',
            chunks: ['vendor']
        }),

    ]

}


var compiler = webpack(webpackConfig);
new WebpackDevServer(compiler, {
    publicPath: "/",
    // compress: true, // Enable gzip compression for everything served，开发时不建议开启
    stats: {
        colors: true
    }
}).listen(3001, "localhost", function(err) {
    if (err) {
        console.log(err);
        return;
    }
});

opn('http://localhost:3001/test.html');
