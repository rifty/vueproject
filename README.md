#vueproject

## 期望：
1、组件按需加载
2、css不单独提取为一个文件，按需加载
3、使用ES6语法
4、css使用sass
5、css可以写在组件中或者通过import方式引入
6、最终打包上线的文件为压缩版本
7、提取公用vendor，加manifest保证vendor的hash不变


## 项目依赖
`node@6.10.0` + `webpack@2.2.1` + `vue@2.2.1` + `vuex@2.2.1` + `vue-router@2.3.0` + `axios@0.15.3` + `ES6` + `babel` + `sass`

## 相关命令

### 安装依赖
```js
npm install // install dependencies
npm install --only=dev // install devDependencies
```

### 开发阶段（development）
```js
npm run dev
```

### 打包阶段（production）
```html
npm run build
```

## Swiper精简为移动端：目前压缩版本为52.40 KB
```js
http://git.oschina.net/rifty/mini-swiper
```

## 项目中如何使用css

### 支持在组件中直接写css
```html
<style>
  ...
</style>
```

### 支持在组件中直接写scss
```html
<style lang="scss"></style>
<!-- 或者 -->
<style lang="sass"></style>
```

### 组件中支持scss的@import
```html
<style lang="scss">
  @import './scss/app.scss'
</style>
```

### 组件中支持scss在js中import
```html
<script>
  import './scss/app.scss'
</script>
```

## webpack2优化

### 配置babel让它在编译转化es6代码时不把import export转换为cmd的module.export
```js
loader: 'babel-loader',
options: {
    presets: [['es2015', {modules: false}]]
}
```






















