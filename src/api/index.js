import Axios from 'axios'


const domain = 'https://cnodejs.org/api/v1/topics'



var getList= function (data, callback) {
        Axios.get(domain, {
                params: data
            })
            .then(response => callback(null,response))
            .catch(error => callback(error));

  }

module.exports = {
    getList
}