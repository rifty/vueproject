import Vue from 'vue'
import router from './router'
import App from './App.vue'

import './scss/app.scss'

import VueTouch from 'vue-touch'
Vue.use(VueTouch, {name: 'v-touch'})
/*

render: h => h(App) 是什么意思？
render函数是渲染一个视图，然后提供给el挂载，如果你没有加那么就等于没有视图给el挂载

这是 es 6 的语法，表示 Vue 实例选项对象的 render 方法作为一个函数，接受传入的参数 h 函数，返回 h(App) 的函数调用结果

Vue 在创建 Vue 实例时，通过调用 render 方法来渲染实例的 DOM 树

Vue 在调用 render 方法时，会传入一个 createElement 函数作为参数，也就是这里的 h 的实参是 createElement 函数，然后 createElement 会以 APP 为参数进行调用

{
    render: h => h(App)
}
等价于
{
    render: h => {
        return h(App)
    }
}
*/
const app = new Vue({
  el: '#app',
  router,
  render: h => h(App)
})

// const app = new Vue({
//   router,
//   render: h => h(App)
// }).$mount('#app')

// new Vue({
//   el: '#app',
//   router,
//   template: '<App/>',
//   components: { App }
// })

/*
换一种写法：
const app = new Vue({
  router,
}).$mount('#app')

此时，需要在html的div里面加入<router-view></router-view>来显示视图
*/

// var vm = new Vue({
//   el: '#app',
//   router,
//   template: '<App/>',
//   components: { App }
// })

/*
第三种写法：
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})

一个字符串模板作为 Vue 实例的标识使用。模板将会 替换 挂载的元素。挂载元素的内容都将被忽略，除非模板的内容有分发 slot。



*/
