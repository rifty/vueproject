import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);


const Index     = () => System.import('../components/index.vue');       // 首页
const TopicView = () => System.import('../components/TopicView.vue');    // 个人中心

// const Index    = resolve => require(['../components/index.vue'], resolve)
// const Activity = resolve => require(['../components/Activity.vue'], resolve)
// const Search   = resolve => require(['../components/Search.vue'], resolve)
// const Personal = resolve => require(['../components/Personal.vue'], resolve)

// const Index = resolve => {
//   require.ensure(['../components/Index.vue'], () => {
//     resolve(require('../components/Index.vue'))
//   })
// }
// const Activity = resolve => {
//   require.ensure(['../components/Activity.vue'], () => {
//     resolve(require('../components/Activity.vue'))
//   })
// }
// const Search = resolve => {
//   require.ensure(['../components/Search.vue'], () => {
//     resolve(require('../components/Search.vue'))
//   })
// }
// const Personal = resolve => {
//   require.ensure(['../components/Personal.vue'], () => {
//     resolve(require('../components/Personal.vue'))
//   })
// }

const routes = [
    {
        path: '/',
        component: Index,
    },
    {
        path: '/index',
        name: 'index',
        component: Index
    },
    {
        path: '/topicview/:tid',
        name: 'topicview',
        component: TopicView
    }
];

export default new VueRouter({
    // mode: 'history',
    routes  // （缩写）相当于 routes: routes
});